package com.formulaone.results;

import java.io.IOException;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.google.gson.Gson;

@Controller
public class AjaxController {
	@RequestMapping("/formula-one/results")
	protected ModelAndView formulaOneResultsDisplay() {
		ModelAndView modelAndView = new ModelAndView("results");
		modelAndView.addObject("welcomeMessage", "Hi Welocome to First Spring MVC Application");
		return modelAndView;
	}

	@RequestMapping("/formula-one/resultset")
	@ResponseBody
	public String getFormulaOneResultSet() {
		WebsiteParser parser = new WebsiteParser();
		GrandPrix grandprix = new GrandPrix();
		String grandPrixResults = "";
		try {
			grandprix.setDrivers(parser.getTopDrivers());
			grandprix.setTeams(parser.getTopTeams());
			Gson gsonbuilder = new Gson();
			grandPrixResults = gsonbuilder.toJson(grandprix);

		} catch (IOException e) {
			e.printStackTrace();
		}
		return grandPrixResults;

	}

}
