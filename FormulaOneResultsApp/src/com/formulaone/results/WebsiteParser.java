package com.formulaone.results;

import java.io.IOException;
import java.util.ArrayList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.StringUtils;

public class WebsiteParser {

	/**
	 * Returns top ten drivers by parsing the f1 si
	 * @return
	 * @throws IOException
	 */
	public ArrayList<String> getTopDrivers() throws IOException {
		String url = "https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/";
		Document doc = Jsoup.connect(url).get();
		String className = "motor-sport-results msr_season_driver_results";
		ArrayList<String> topDrivers = new ArrayList<String>();
		if (doc.select("table").hasClass(className)) {
			Elements tableElement = doc.getElementsByClass(className);
			Elements tableRowElements = tableElement.select(":not(thead) tr");
			System.out.println("body");
			int index = 0;
			for (Element row : tableRowElements) {
				Elements rowItems = row.select("td");
				for (Element column : rowItems) {
					if (column.hasClass("msr_driver")) {
						topDrivers.add(column.getElementsByTag("a").text());
						index++;
					}
					if (index > 9) {
						break;
					}
				}
				if (index > 9) {
					break;
				}
			}
		}
		return topDrivers;

	}

	/**
	 * Returns the top ten teams by parsing the f1 site
	 * @return
	 * @throws IOException
	 */
	public ArrayList<String> getTopTeams() throws IOException {
		String url = "https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/";
		Document doc = Jsoup.connect(url).get();
		String className = "motor-sport-results msr_season_team_results";
		ArrayList<String> topTeams = new ArrayList<String>();
		if (doc.select("table").hasClass(className)) {
			Elements tableElement = doc.getElementsByClass(className);
			Elements tableRowElements = tableElement.select("thead tr");
			System.out.println("head");
			for (Element row : tableRowElements) {
				int index = 0;
				Elements rowItems = row.select("th");
				for (Element column : rowItems) {
					String teamName = column.getElementsByTag("a").text();
					if (!StringUtils.isEmpty(teamName)) {
						topTeams.add(teamName);
						index++;
					}
					if (index > 4) {
						break;
					}
				}
				if (index > 4) {
					break;
				}
				
			}
		}
		return topTeams;

	}
}
