package com.formulaone.results;

import java.util.ArrayList;

public class GrandPrix {
	ArrayList<String> drivers;
	ArrayList<String> teams;

	public ArrayList<String> getDrivers() {
		return drivers;
	}

	public void setDrivers(ArrayList<String> drivers) {
		this.drivers = drivers;
	}

	public ArrayList<String> getTeams() {
		return teams;
	}

	public void setTeams(ArrayList<String> teams) {
		this.teams = teams;
	}

}
