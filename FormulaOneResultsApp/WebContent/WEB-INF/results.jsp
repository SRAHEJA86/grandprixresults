<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html;charset=UTF-8">
<script
	src="http://ajax.googleapis.com/ajax/libs/jquery/1.5.2/jquery.min.js"></script>
<script type="text/javascript">
		$(document).ready(function(){
				$.ajax({
					url:"resultset",
					dataType:"json",
					success:function(data){
						var obj = $.parseJSON(JSON.stringify(data));
						 for(var i=0;i<obj.drivers.length;i++)
						    {
						        var tr="<tr>";
						        var td1="<td>"+obj.drivers[i]+"</td>";
						        var td2 ="";
						        if(typeof obj.teams[i] !== 'undefined'){
						        	td2="<td>"+obj.teams[i]+"</td>";
						        }
						        var trend = "</tr>";
						       	$("#grandprix_results").append(tr+td1+td2+trend); 
						    }
					}
					});
					
				//});
		});
	</script>

</head>
<body>
	<h1>Formula One Results 2018</h1>
	<table id="grandprix_results">
		<tr>
			<th>Top Drivers</th>
			<th>Top Teams</th>
		</tr>
	</table>
	<p id="id_grand_prix_results"></p>
</body>
</html>