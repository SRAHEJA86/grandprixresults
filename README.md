This project is the FormulaOneResultsApp thas has been created using Spring MVC on JAVA 8 using Tomcat Apache server as the application server

To execute the project please follow the steps below:
Import the git project to local eclipse
Publish the project folder FormulaOneResultsAppto to Apache Tomcat server
Hit the URL on any webbrowser at local http://localhost:8080/FormulaOneResultsApp/formula-one/results
User can see the Grand Prix  2018 results - top ten drivers and top five teams populated in tabulatr form


The project contains the following files:

Model 
======

1. WebsiteParser.java
-------------------
The main logic of parsing the formula one site- 
https://www.f1-fansite.com/f1-results/2018-f1-championship-standings/
has been written in Website Parser.java. The parsing has been done using Jsoup library

2. GrandPrix.java
---------------
It is a simple POJO object for populating the top ten drivers and top five teams

View 
==========
The formula one results are then displayed at the front-end using results.jsp
When a user hits a URL http://localhost:8080/FormulaOneResultsApp/formula-one/results, and ajax call is made to the server 
to fetch the top ten drivers and top five teams from the online html and populate at the front end using javascript


Controller
===========
The Ajax Controller is the controller class